﻿/*
 Name:		VoltAmper_Metr.ino
 Created:	09.11.2019 20:44:24
 Author:	Алексей
*/

// the setup function runs once when you press reset or power the board
#define NUM_OF_DIGITS (4)
//const int control_pins[4] = { 4, 5, 6, 7 };
const int control_pins[4] = { 7, 6, 5, 4 };
int latchPin = 12;
//Пин подключен к SH_CP входу 74HC595
int clockPin = 8;
//Пин подключен к DS входу 74HC595
int dataPin = 9;
const char common = 'a';    // common anode
unsigned int cnt = 0;
byte digit_pattern[10] = {
	B01111011, // 0
	B00010010, // 1
	B01100111, // 2
	B00110111, // 3
	B00011110, // 4
	B00111101, // 5
	B01111101, // 6
	B00010011, // 7
	B01111111, // 8
	B00111111, // 9
};
int digit_data[NUM_OF_DIGITS] = { 0 };
int scan_position = 0;

unsigned int counter = 0;
void update_one_digit()
{
	int i;
	byte pattern;

	// turn off all digit
	for (i = 0; i < NUM_OF_DIGITS; i++){
		digitalWrite(control_pins[i], LOW);
	}

	// get the digit pattern of the position to be updated
	pattern = digit_pattern[digit_data[scan_position]];

	// turn off the output of 74HC595
	digitalWrite(latchPin, LOW);

	// update data pattern to be outputed from 74HC595
	// because it's a common anode LED, the pattern needs to be inverted
	shiftOut(dataPin, clockPin, LSBFIRST, ~pattern);

	// turn on the output of 74HC595
	digitalWrite(latchPin, HIGH);

	// turn on the digit to be updated in this round
	digitalWrite(control_pins[scan_position], HIGH);

	// go to next update position
	scan_position++;

	if (scan_position >= NUM_OF_DIGITS){
		scan_position = 0;
	}
}

void setup() {
	pinMode(latchPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(dataPin, OUTPUT);

	for (uint8_t i = 0; i < NUM_OF_DIGITS; i++)
	{
		pinMode(control_pins[i], OUTPUT);
	}
	digit_data[0] = 4;
	digit_data[1] = 8;
	digit_data[2] = 6;
	digit_data[3] = 9;
}

// the loop function runs over and over again until power down or reset
void loop() {
	int i;
	unsigned int number;
	unsigned int digit_base;

	counter++;

	// get the value to be displayed
	number = counter / 100;

	digit_base = 10;

	// get every values in each position of those 4 digits based on "digit_base"
	//
	// digit_base should be <= 16
	//
	// for example, if digit_base := 2, binary values will be shown. If digit_base := 16, hexidecimal values will be shown
	//
	/*for (i = 0; i < NUM_OF_DIGITS; i++)
	{
		digit_data[i] = number % digit_base;
		number /= digit_base;
	}*/

	// update one digit
	update_one_digit();

	delay(4);
	
}

void myfnUpdateDisplay(byte eightBits) {
	if (common == 'a') {                  // using a common anonde display?
		eightBits = eightBits ^ B11111111;  // then flip all bits using XOR 
	}
	digitalWrite(latchPin, LOW);  // prepare shift register for data
	shiftOut(dataPin, clockPin, LSBFIRST, eightBits); // send data
	digitalWrite(latchPin, HIGH); // update display
}

byte myfnNumToBits(int someNumber) {
	switch (someNumber) {
	case 0:
		return B01111011;
		break;
	case 1:
		return B00010010;
		break;
	case 2:
		return B01100111;
		break;
	case 3:
		return B00110111;
		break;
	case 4:
		return B00011110;
		break;
	case 5:
		return B00111101;
		break;
	case 6:
		return B01111101;
		break;
	case 7:
		return B00010011;
		break;
	case 8:
		return B01111111;
		break;
	case 9:
		return B00111111;
		break;
	//case 10:
	//	return B11101110; // Hexidecimal A
	//	break;
	//case 11:
	//	return B00111110; // Hexidecimal B
	//	break;
	//case 12:
	//	return B10011100; // Hexidecimal C or use for Centigrade
	//	break;
	//case 13:
	//	return B01111010; // Hexidecimal D
	//	break;
	//case 14:
	//	return B10011110; // Hexidecimal E
	//	break;
	//case 15:
	//	return B10001110; // Hexidecimal F or use for Fahrenheit
	//	break;
	default:
		return B10010010; // Error condition, displays three vertical bars
		break;
	}
}
